import { http } from './config'

// Este é o meu sufixo
export default{
    // Método para listar items
    listar:() => {
        return http.get('Livros')
    },
   
    // Métodos para salvar items
    add:(Livros) => {
        return http.post('Livros', Livros)
    },
    edit:(Livros) => {
        return http.put('Livros', Livros)
    },
    delete_:(Livros) => {
        return http.delete('Livros', {data: Livros})
    }

}